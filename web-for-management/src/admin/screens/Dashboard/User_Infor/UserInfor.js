import Addresspng from '../../../screens/image/address.png'
import Emailpng from '../../../screens/image/email.png'
import Phonepng from '../../../screens/image/phone.png'
import Avtpng from '../../../screens/image/Avater.jpg'
import logout from '../../image/logout.png'
import changepasspng from '../../image/sync-alt.png'
import './UserInfor.css'

function UserInfor(){
    return(
        <div>
            <ProFile
            avatar = {Avtpng}
            name = "Nguyễn Anh Dũng"
            tag = "Admin in Ecopark BikeRenting"
            phoneNumber = "0972873688"
            email = "dung.na194255@sis.hust.edu.vn"
            idCode = "12345689"
            >
            </ProFile>
        </div>
    )
}

function ProFile(props){
    const avatar= props.avatar
    const name = props.name
    const tag = props.tag
    const phoneNumber = props.phoneNumber
    const email = props.email
    const idCode = props.idCode
    return(
        <div className="userInfor">
            <img id ="Avartar" src={avatar} alt=""/>
            <div className="name">
                <h1 className="fullName">{name}</h1>
                <span className ="tag">{tag}</span>
            </div>
            <button className="changePass">
                <span className="insideChangePass">Change password</span>
                <img id="changepass" src={changepasspng} alt=""/>
            </button>
            <button className="logOut">
                <span className="insideLogOut">Logout</span>
                <img id="LogOut" src={logout} alt=""/>
            </button>
            <div className="idDivision">
                <img className="image" src={Addresspng} alt=""/>
                <span className="titleOfInfor">Identity Code</span>
                <span className="detail">{idCode}</span>
            </div>
            <div className="emailDivision">
                <img className="image" src={Emailpng} alt=""/>
                <span className="titleOfInfor">Email</span>
                <span className="detail">{email}</span>
            </div>
            <div className="phoneDivision">
                <img className="image" src={Phonepng} alt=""/>
                <span className="titleOfInfor">Phone Number</span>
                <span className="detail">{phoneNumber}</span>
            </div>
        </div>
    )
}
export default UserInfor