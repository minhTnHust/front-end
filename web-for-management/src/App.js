import './App.css';
import React, { Component, useState } from 'react';
import Dashboard from './admin/screens/Dashboard/Dashboard';
import MenuBar from './admin/shared/component/MenuBar/MenuBar';


function App() {
  const [screen, setScreen] = useState(1)
  function changeScreen(value) {
    setScreen(value)
    console.log(value)
  }
  return (
    <div>
      <MenuBar callBack={changeScreen}></MenuBar>
      {
        (screen == 2) ? <ListEmployee></ListEmployee> :
          (screen == 3) ? <ListStation></ListStation> :
            (screen == 4) ? <ListStaff></ListStaff> : <Dashboard></Dashboard>
      }
    </div>
  );
}

function ListEmployee() {
  return <div style={{ position: "static", left: 500, height: 100, width: 1000, backgroundColor: "red" }}>
  </div>
}
function ListStation() {
  return <div style={{ left: 400, height: 100, width: 1000, backgroundColor: "green" }}>
  </div>
}
function ListStaff() {
  return <div style={{ left: 400, height: 100, width: 1000, backgroundColor: "blue" }}>
  </div>
}
export default App;
