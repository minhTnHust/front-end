var check = [true, false, false];
changeButton = () => {
    var button1 = document.getElementById("page1");
    var button2 = document.getElementById("page2");
    var button3 = document.getElementById("page3");
    if (check[0] == true) button1.className = "selected-page-button";
    else button1.className = "unselected-page-button";
    if (check[1] == true) button2.className = "selected-page-button";
    else button2.className = "unselected-page-button";
    if (check[2] == true) button3.className = "selected-page-button";
    else button3.className = "unselected-page-button";
}
switchButton = async (index) => {
    var res = await fetch("https://testapi.io/api/minhtrannhat/info/" + index.toString(), {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        },
    });
    var data = await res.json();
    for (var i = 0; i < data.length; i++) {
        console.log(data[i]["avatar"]);
        var avatarAndNames = document.getElementsByClassName("avatar-and-infor");
        for (var i = 0; i < avatarAndNames.length; i++) {
            var image = avatarAndNames[i].getElementsByClassName("image");
            var name = avatarAndNames[i].getElementsByClassName("info");
            var email = avatarAndNames[i].getElementsByClassName("email");
            image[0].setAttribute("src", data[i]["avatar"]);
            name[0].innerHTML = data[i]["name"];
            email[0].innerHTML = data[i]["email"];
        }
        var invoices = document.getElementsByClassName("invoice");
        for (var i = 0; i < invoices.length; i++) {
            invoices[i].innerHTML = data[i]["_id"];
        }
        var dates = document.getElementsByClassName("date");
        for (var i = 0; i < dates.length; i++) {
            dates[i].innerHTML = data[i]["date"];
        }
        var amounts = document.getElementsByClassName("amount");
        for (var i = 0; i < amounts.length; i++) {
            amounts[i].innerHTML = data[i]["amount"];
        }
        var locations = document.getElementsByClassName("location");
        for (var i = 0; i < amounts.length; i++) {
            locations[i].innerHTML = data[i]["location"];
        }
        var statuss = document.getElementsByClassName("check");
        for (var i = 0; i < statuss.length; i++) {
            if (data[i]["status"] == true) {
                var button = statuss[i].children;
                button[0].className = "status";
                button[0].innerHTML = "Complete";
            } else {
                var button = statuss[i].children;
                button[0].className = "unstatus";
                button[0].innerHTML = "Uncomplete";
            }
        }
    }

    index--;
    for (var i = 0; i < 3; i++) {
        if (index == i) {
            check[i] = true;
        } else {
            check[i] = false;
        }
    }
    console.log(check);
    changeButton();
}
switchButton(1);